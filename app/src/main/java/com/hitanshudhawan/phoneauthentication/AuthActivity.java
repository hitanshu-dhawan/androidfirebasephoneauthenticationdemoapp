package com.hitanshudhawan.phoneauthentication;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class AuthActivity extends AppCompatActivity {

    private TextView mobileNumberTextView;
    private EditText mobileNumberEditText;

    private TextView verificationCodeTextView;
    private EditText verificationCodeEditText;

    private Button verifyButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        mobileNumberTextView = (TextView) findViewById(R.id.mobileNumberTextView);
        mobileNumberEditText = (EditText) findViewById(R.id.mobileNumberEditText);

        verificationCodeTextView = (TextView) findViewById(R.id.verificationCodeTextView);
        verificationCodeEditText = (EditText) findViewById(R.id.verificationCodeEditText);

        verifyButton = (Button) findViewById(R.id.verifyButton);
        verifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String mobileNumber = mobileNumberEditText.getText().toString();
                PhoneAuthProvider.getInstance().verifyPhoneNumber(mobileNumber, 60, TimeUnit.SECONDS, AuthActivity.this, new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                        signInWithPhoneAuthCredential(phoneAuthCredential);
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        //
                    }

                    @Override
                    public void onCodeSent(final String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        mobileNumberTextView.setEnabled(false);
                        mobileNumberEditText.setEnabled(false);
                        verificationCodeTextView.setVisibility(View.VISIBLE);
                        verificationCodeEditText.setVisibility(View.VISIBLE);

                        verifyButton.setText("Verify Code");
                        verifyButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, verificationCodeEditText.getText().toString());
                                signInWithPhoneAuthCredential(credential);
                            }
                        });
                    }

                    @Override
                    public void onCodeAutoRetrievalTimeOut(String verificationId) {
                        //
                    }
                });
            }
        });
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(AuthActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // everything is good.
                    Intent mainIntent = new Intent(AuthActivity.this, MainActivity.class);
                    startActivity(mainIntent);
                    finish();
                }
            }
        });
    }
}
